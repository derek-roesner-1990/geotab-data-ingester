import json
import requests
import sys
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

# Raises an exception on error.
def send_data(data, destination_url, timeout_seconds):

	# See these resources for more details:
	# https://www.peterbe.com/plog/best-practice-with-retries-with-requests
	# https://urllib3.readthedocs.io/en/latest/reference/urllib3.util.html#module-urllib3.util.retry
	# http://docs.python-requests.org/en/master/api/#requests.adapters.HTTPAdapter
	def requests_retry_session():

		# For a total of 5 attempts.
		RETRY_AMT = 4

		# urllib3 will sleep for: {backoff factor} * (2 ^ ({number of total retries} - 1)) seconds between each attempt.
		BACKOFF_FACTOR = 0.5

		# The HTTP method verbs to retry on.
		METHOD_WHITELIST = frozenset(['POST', 'OPTIONS'])

		# The HTTP status codes to force a retry on.
		# 429 - Too many requests (i.e: back off)
		# 500 - Generic internal server error
		# 502 - Bad Gateway - i.e: upstream failure
		# 503 - Temporarily Unavailable
		# 504 - Gateway timeout
		# 522 - Origin connection timed out
		STATUS_FORCELIST = [429, 500, 502, 503, 504, 522]

		session = requests.Session()

		retry = Retry(
			total=RETRY_AMT,
			read=RETRY_AMT,
			connect=RETRY_AMT,
			status=RETRY_AMT,
			backoff_factor=BACKOFF_FACTOR,
			status_forcelist=STATUS_FORCELIST,
			method_whitelist = METHOD_WHITELIST,
			raise_on_status = False,
			raise_on_redirect = False)

		adapter = HTTPAdapter(max_retries=retry)

		session.mount('http://', adapter)
		session.mount('https://', adapter)

		return session

	try:
		
		print "\n- Sending data to {}.".format(destination_url)

		headers = {'content-type': 'application/json'}

		# More info on use of json.dumps()
		# https://docs.python.org/2/library/json.html#json.dumps
		data_as_json = json.dumps(data, default=str)
		#print data_as_json

		# See documentation @ http://docs.python-requests.org/en/master/user/quickstart/#make-a-request for more details on use of post().
		response = requests_retry_session().post(destination_url,timeout=timeout_seconds,headers=headers,json=data_as_json)

		print response.content

		# Raises a "http_error" if the status code of the response indicates a 4XX client error or 5XX server error.  
		response.raise_for_status()

		print "- Data successfully sent."
	
	except Exception as e:
		raise Exception("An error occured while sending data to {}.\nDetails:\n  {}".format(destination_url,e))
	
	return response

def main():

	# URL of the "geotab-weather-data-ingester-api-service" that is running within the Kubernetes cluster associated with the weathertx2-staging project.
	ROOT_URL = "http://35.226.27.171:5000/"
	# URL of this api running within a local Docker container.
	#ROOT_URL = "http://127.0.0.1:5000/"

	TEMPERATURE_ENDPOINT = "{}{}".format(ROOT_URL,"ingest_temperature_data/")
	BAROMETRIC_PRESSURE_ENDPOINT = "{}{}".format(ROOT_URL,"ingest_pressure_data/")

	try:

		# Temperature Data
		data = []
		data.append({"Latitude_SW": 44.34219, "Latitude_NE": 44.34357, "UTC_Hour": "11", "Temperature_C": 23.4, "Longitude_NE": -69.79889, "Longitude_SW": -69.80026, "UTC_Date": "2018-07-31"})
		send_data(data, TEMPERATURE_ENDPOINT, timeout_seconds=60)

		# Air Pressure Data
		data = []
		data.append({'MSLP': 102300, 'Latitude_SW': 44.30786, 'Latitude_NE': 44.31335, 'UTC_Hour': u'11', 'Pressure': 102000, 'Longitude_NE': -69.77417, 'Longitude_SW': -69.78516, 'UTC_Date': u'2018-07-31'})
		send_data(data, BAROMETRIC_PRESSURE_ENDPOINT, timeout_seconds=60)

	except Exception as e:
		print >>sys.stderr, e
		sys.exit(1)

#https://stackoverflow.com/questions/419163/what-does-if-name-main-do#419185
if __name__ == '__main__':
    main()

