import arrow
import json
import requests
import sys
import time
from google.cloud import bigquery #pip2 install --upgrade --user google-cloud-bigquery
from google.oauth2 import service_account
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

# Returns None on error.
def authenticate(project_id, client_secret):

	try:

		print "\n- Attempting to authenticate with the Google BigQuery project: '{}'.".format(project_id)

		# See this resource for more information on authenticating with the Google BigQuery API using a service account.
		# https://www.blendo.co/blog/access-data-google-bigquery-python-r/
		credentials = service_account.Credentials.from_service_account_file(client_secret)
		client = bigquery.Client(project=project_id, credentials=credentials)

		print "- Authentication successful."

	except Exception as e:
		print >>sys.stderr, "\nAn error occured during authentication.\nDetails:\n  {}".format(e)
		client = None

	return client

# Raises an exception on error.
def prepare_query(query, utc_date, utc_hour):

	try:

		# More info on creating a parameterized query when using Google BigQuery.
		# https://googlecloudplatform.github.io/google-cloud-python/latest/bigquery/usage.html#run-a-query-using-a-named-query-parameter

		print "\n- Preparing query."

		queryInfo = { 'query': None, 'query_job_config': None }
		query_params = [ bigquery.ScalarQueryParameter('utc_date', 'DATE', utc_date), bigquery.ScalarQueryParameter('utc_hour', 'STRING', utc_hour) ]

		job_config = bigquery.QueryJobConfig()
		job_config.query_parameters = query_params

		queryInfo['query'] = query
		queryInfo['query_job_config'] = job_config

		print "- Query prepared."

	except Exception as e:
		raise Exception("\nAn error occured while preparing the query.\nDetails:\n  {}".format(e))

	return queryInfo

# Raises an exception on error.
def query_data_set(client, query_info):

	try:

		print "\n- Attempting to retrieve data."
		print "Query Ran At: {} UTC".format(arrow.utcnow().format('YYYY-MM-DD HH:mm:ss'))
		print "Query Used:\n  {}\nParameters Applied to Query:\n  {}.".format(query_info['query'], query_info['query_job_config'].query_parameters)

		query_job = client.query(query_info['query'], job_config = query_info['query_job_config'])

		# Start the job, wait for it to complete then get the result.
		# See the resource below for more info on the returned object.
		# https://googlecloudplatform.github.io/google-cloud-python/latest/bigquery/generated/google.cloud.bigquery.table.RowIterator.html#google-cloud-bigquery-table-rowiterator
		row_iterator = query_job.result()

		print "- Result of query has been retrieved."

	except Exception as e:
		raise Exception("\nAn errror occured while retrieving data.\nDetails:\n  {}".format(e))

	return row_iterator

# Retrieving the results from the query as groups and sending each group to the specified endpoint.
# Raises an exception on error.
def send_data_in_groups(endpoint, row_iterator, group_size):

	try:

		print "\n- Processing result of the query."
		print "- Weather observations will be sent to '{}'.".format(endpoint)

		data_in_group = []
		group_data_count = 0
		rows_data_count = 0

		for row in row_iterator:

			data = {}

			for key in list(row.keys()):
				data[key] = row.get(key)

			data_in_group.append(data)

			group_data_count += 1
			rows_data_count += 1

			# If the maximum group size or the last row of data has been reached.
			if(group_data_count == group_size or rows_data_count == row_iterator.total_rows):
				send_data(data_in_group, endpoint, timeout_seconds = 60)
				data_in_group = []
				group_data_count = 0

		if rows_data_count == 0:
			print "- No weather observations were retrieved by the query."
		else:
			print "- {} weather observations have been successfully sent to '{}'.".format(rows_data_count, endpoint)

	except Exception as e:
		raise Exception("An error occured while processing the retrieved data.\nDetails:\n  {}".format(e))

	return rows_data_count

# Raises an exception on error.
def send_data(data, destination_url, timeout_seconds):

	# See these resources for more details:
	# https://www.peterbe.com/plog/best-practice-with-retries-with-requests
	# https://urllib3.readthedocs.io/en/latest/reference/urllib3.util.html#module-urllib3.util.retry
	# http://docs.python-requests.org/en/master/api/#requests.adapters.HTTPAdapter
	def requests_retry_session():

		# For a total of 5 attempts.
		RETRY_AMT = 4

		# urllib3 will sleep for: {backoff factor} * (2 ^ ({number of total retries} - 1)) seconds between each attempt.
		BACKOFF_FACTOR = 0.5

		# The HTTP method verbs to retry on.
		METHOD_WHITELIST = frozenset(['POST', 'OPTIONS'])

		# The HTTP status codes to force a retry on.
		# 429 - Too many requests (i.e: back off)
		# 500 - Generic internal server error
		# 502 - Bad Gateway - i.e: upstream failure
		# 503 - Temporarily Unavailable
		# 504 - Gateway timeout
		# 522 - Origin connection timed out
		STATUS_FORCELIST = [429, 500, 502, 503, 504, 522]

		session = requests.Session()

		retry = Retry(
			total=RETRY_AMT,
			read=RETRY_AMT,
			connect=RETRY_AMT,
			status=RETRY_AMT,
			backoff_factor=BACKOFF_FACTOR,
			status_forcelist=STATUS_FORCELIST,
			method_whitelist = METHOD_WHITELIST,
			raise_on_status = False,
			raise_on_redirect = False)

		adapter = HTTPAdapter(max_retries=retry)

		session.mount('http://', adapter)
		session.mount('https://', adapter)

		return session

	try:

		headers = {'content-type': 'application/json'}

		# More info on use of json.dumps()
		# https://docs.python.org/2/library/json.html#json.dumps
		data_as_json = json.dumps(data, default=str)
		#print data_as_json

		# See documentation at http://docs.python-requests.org/en/master/user/quickstart/#make-a-request for more details on use of post().
		response = requests_retry_session().post(destination_url,timeout=timeout_seconds,headers=headers,json=data_as_json)

		#print response.content

		# Raises a "http_error" if the status code of the response indicates a 4XX client error or 5XX server error.  
		response.raise_for_status()

	except Exception as e:
		raise Exception("An error occured while sending data to '{}'.\nDetails:\n  {}".format(destination_url,e))

	return response

def main():

	GOOGLE_BIGQUERY_PROJECT_ID = 'geotab-intelligence'
	SERVICE_ACCOUNT_KEY = '../geotab-intelligence-key.json'
	# External URL for the "geotab-weather-data-ingester-api-service" that is running within the Kubernetes cluster associated with the weathertx2-staging project.
	#ROOT_URL = "http://35.226.27.171:5000/"
	# Internal URL for the "geotab-weather-data-ingester-api-service". This can be used when the data ingester is running with the same Kubernetes cluster.
	ROOT_URL = "http://geotab-weather-data-ingester-api-service:5000/"
	TEMPERATURE_ENDPOINT = "{}{}".format(ROOT_URL,"ingest_temperature_data/")
	BAROMETRIC_PRESSURE_ENDPOINT = "{}{}".format(ROOT_URL,"ingest_pressure_data/")
	MAX_GROUP_SIZE = 1000
	QUERY_ATTEMPT_MAX = 6
	QUERY_RETRY_WAIT = 300 # 5 minutes in seconds
	HOUR_OFFSET = -1
	QUERY_DATE_TIME = arrow.utcnow().shift(hours=HOUR_OFFSET)

	#The value returned when the script exits. Will be set to 1 if an error occurs while trying to complete a task detailed in TASK_INFO.
	return_value = 0

	# Query to retrieve air temperature, datetime, and location data from the geotab-intelligence.Weather dataset.

	# Use in production / staging.
	TEMPERATURE_QUERY = "SELECT Longitude_SW, Longitude_NE, Latitude_SW, Latitude_NE, Temperature_C, UTC_Date, UTC_Hour FROM `geotab-intelligence.Weather.Temperature` WHERE Country = 'United States of America (the)' AND UTC_Date = @utc_date AND UTC_HOUR = @utc_hour"

	# Use during development / testing.
	#TEMPERATURE_QUERY = "SELECT Longitude_SW, Longitude_NE, Latitude_SW, Latitude_NE, Temperature_C, UTC_Date, UTC_Hour FROM `geotab-intelligence.Weather.Temperature` WHERE Country = 'United States of America (the)' AND State = 'Maine' AND City = 'Augusta' AND UTC_Date = @utc_date AND UTC_HOUR = @utc_hour"

	# Query to retrieve atmospheric pressure, mean sea level adjusted pressure, datetime, and location data from the geotab-intelligence.Weather dataset.

	# Use in production / staging.
	BAROMETRIC_PRESSURE_QUERY = "SELECT Longitude_SW, Longitude_NE, Latitude_SW, Latitude_NE, Pressure, MSLP, UTC_Date, UTC_Hour FROM `geotab-intelligence.Weather.Pressure` WHERE Country = 'United States of America (the)' AND UTC_Date = @utc_date AND UTC_HOUR = @utc_hour"

	# Use during development / testing.
	#BAROMETRIC_PRESSURE_QUERY = "SELECT Longitude_SW, Longitude_NE, Latitude_SW, Latitude_NE, Pressure, MSLP, UTC_Date, UTC_Hour FROM `geotab-intelligence.Weather.Pressure` WHERE Country = 'United States of America (the)' AND State = 'Maine' AND City = 'Augusta' AND UTC_Date = @utc_date AND UTC_HOUR = @utc_hour"


	# Use in production or when testing retrieval and sending of both temperature and air pressure data.
	TASK_INFO = [{ 'query': TEMPERATURE_QUERY, 'endpoint': TEMPERATURE_ENDPOINT }, { 'query': BAROMETRIC_PRESSURE_QUERY, 'endpoint': BAROMETRIC_PRESSURE_ENDPOINT }]

	# Used while testing only the retrieval and sending of temperature data.
	#TASK_INFO = [{ 'query': TEMPERATURE_QUERY, 'endpoint': TEMPERATURE_ENDPOINT }]

	# Used while testing only the retrieval and sending of air pressure data.
	#TASK_INFO = [{ 'query': BAROMETRIC_PRESSURE_QUERY, 'endpoint': BAROMETRIC_PRESSURE_ENDPOINT }]

	# Retrieve an access token using OAuth2 to interact with the intended project via the Google BigQuery API.
	# The renewal of the access token will be handled by the client object.
	client = authenticate(GOOGLE_BIGQUERY_PROJECT_ID, SERVICE_ACCOUNT_KEY)

	if(client == None):
		sys.exit(1)

	# Now that authentication is complete attempt to perform the following actions:
	# 	Build and run a query against the dataset within the intended Google BigQuery project.
	# 	Convert the result to JSON and send to the intended endpoint for processing.

	for task in TASK_INFO:
		try:
			task_start = arrow.utcnow()

			rows_retrieved = 0
			attempt_count = 1

			# Data retrieval will be attempted 6 times for a total of 30 minutes per task.
			# See QUERY_ATTEMPT_MAX and QUERY_RETRY_WAIT for more information.
			while rows_retrieved == 0:
				query_info = prepare_query(task['query'], QUERY_DATE_TIME.date(), QUERY_DATE_TIME.hour)
				row_iterator = query_data_set(client, query_info)

				# The amount of rows is not known until the interator is accessed within send_data_in_groups().
				# It would be preferred to know this value before hand but the interator will throw an error if accessed twice.
				rows_retrieved = send_data_in_groups(task['endpoint'], row_iterator, MAX_GROUP_SIZE)

				if rows_retrieved == 0:
					if attempt_count < QUERY_ATTEMPT_MAX:
						print "- Waiting {} minutes and trying again.".format(QUERY_RETRY_WAIT/60)
						time.sleep(QUERY_RETRY_WAIT)
						attempt_count += 1
					else:
						print "- The retry limit of {} has been reached. Abandoning task.".format(QUERY_ATTEMPT_MAX)
						break

			task_end = arrow.utcnow()
			print "- Time taken to complete task: {}".format(task_end-task_start)
		except Exception as e:
			return_value = 1
			print >>sys.stderr, e

	sys.exit(return_value)

# More info at https://stackoverflow.com/questions/419163/what-does-if-name-main-do#419185
if __name__ == '__main__':
    main()

