import calendar
import logging
import subprocess
import sys
import time
from apscheduler.schedulers.blocking import BlockingScheduler
from pytz import utc

def start_new_process(process_arguments):
	try:
		new_process = { 'process': None, 'start_time': 0, 'seconds_since_start': 0 }
		# More info on Popen() - https://docs.python.org/2/library/subprocess.html#subprocess.Popen
		new_process['process'] = subprocess.Popen(process_arguments)
		new_process['start_time'] = calendar.timegm(time.gmtime())
		print "Started new process {} using the arguments {}.".format(new_process['process'].pid, process_arguments)
	except Exception as e:
		raise Exception("An error occured while starting a new process.\nDetails:\n  {}".format(e))

	return new_process

def check_state_of_processes(processes, max_seconds_to_live):

	try:

		i = len(processes)

		if i > 0:
			print "Checking the state of the processes.\n"

			while i > 0:
				i -= 1
				process_info = processes[i]
				status = process_info['process'].poll()

				if status is None:
					state_descriptor = "Running"
					return_code_info = ""
				else:
					state_descriptor = "Returned"
					return_code_info = "\nReturn Code: {}".format(status)

				process_info['seconds_since_start'] = calendar.timegm(time.gmtime()) - process_info['start_time']
				print "Process Id: {}\nSeconds Since Start: {}\nState: {}{}".format(process_info['process'].pid, process_info['seconds_since_start'], state_descriptor, return_code_info)

				if status is None:
					if process_info['seconds_since_start'] >= max_seconds_to_live:
						print "The process with id of {} has been running too long. Killing process.".format(process_info['process'].pid)
						process_info['process'].kill()
						del processes[i]
				else:
					if status != 0:
						print "This process has returned in an error state."
					del processes[i]

				print ""

			print "Finished checking the state of the processes."

	except Exception as e:
		raise Exception("An error occured while checking the state of the processes.\nDetails:\n  {}".format(e))

	return processes

def hourly_task(processes, process_arguments, max_seconds_for_process):
	try:
		print "--------------------------------------------------------------------"
		processes.append(start_new_process(process_arguments))
		processes = check_state_of_processes(processes, max_seconds_for_process)
		print "--------------------------------------------------------------------"
		print "Output from running process."
	except Exception as e:
		raise e

def main():

	processes = []

	TIME_TO_LIVE_MULTIPLIER = 2
	MAX_SECONDS_FOR_PROCESS = int(sys.argv[1]) * TIME_TO_LIVE_MULTIPLIER
	PROCESS_ARGUMENTS = sys.argv[2:]
	MINUTE_TO_START_PROCESS = '59'
	SECOND_TO_START_PROCESS = '55'

	# Allowing logging messages produced by the scheduler to be output to the console.
	log = logging.getLogger('apscheduler.executors.default')
	log.setLevel(logging.INFO)  # DEBUG

	# Allowing logging messages from stdout and stderr to be output to the console.
	fmt = logging.Formatter('%(message)s')
	h = logging.StreamHandler()
	h.setFormatter(fmt)
	log.addHandler(h)

	# Scheduling the function hourly_task() to be ran at the last 5 seconds of every hour.
	# More info - https://apscheduler.readthedocs.io/en/latest/
	scheduler = BlockingScheduler(timezone=utc)
	scheduler.add_job(hourly_task, 'cron', [processes, PROCESS_ARGUMENTS, MAX_SECONDS_FOR_PROCESS], day_of_week='mon-sun', hour='0-23', minute=MINUTE_TO_START_PROCESS, second=SECOND_TO_START_PROCESS)

	print "The process scheduler has started."
	print "A process is scheduled to start each hour at 'HH:{}:{}'.".format(MINUTE_TO_START_PROCESS, SECOND_TO_START_PROCESS)

	try:
		scheduler.start()
	except Exception as e:
		print >>sys.stderr, e

# More info at https://stackoverflow.com/questions/419163/what-does-if-name-main-do#419185
if __name__ == '__main__':
    main()

