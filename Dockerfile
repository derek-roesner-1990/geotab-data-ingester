FROM ubuntu:latest
RUN apt-get update -y #redo
RUN apt-get install -y python-pip python-dev build-essential
COPY required_python_modules.txt /app/required_python_modules.txt
RUN pip install -r /app/required_python_modules.txt
COPY . /app
WORKDIR /app/src
CMD ["python", "-u", "process_scheduler.py", "3600", "python", "data_ingester.py"]

